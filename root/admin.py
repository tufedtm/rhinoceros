from django.contrib import admin
from root.models import *

admin.site.register(Faq)
admin.site.register(ContactForm)
admin.site.register(Works)
admin.site.register(WorksType)
