# coding: utf8

from __future__ import unicode_literals
from django.db import models
from stdimage.models import StdImageField


class Faq(models.Model):
    question = models.CharField('Вопрос', max_length=200)
    answer = models.TextField('Ответ', max_length=10000)

    def __unicode__(self):
        return self.question

    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = 'FAQ'


class ContactForm(models.Model):
    name = models.CharField('Имя', max_length=100)
    phone = models.CharField('Телефон', max_length=100)
    message = models.TextField('Соббщение', max_length=10000)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Заявка с сайта'
        verbose_name_plural = 'Заявки с сайта'


class WorksType(models.Model):
    title = models.CharField('Тип работы', max_length=50)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Тип работы'
        verbose_name_plural = 'Типы работ'


class Works(models.Model):
    title = models.CharField('Название', max_length=100)
    img = StdImageField('Фото', upload_to='works', variations={
        'thumb': (270, 189, True)
    })
    worksType = models.ForeignKey(WorksType, related_name='workstype')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Работа'
        verbose_name_plural = 'Работы'
