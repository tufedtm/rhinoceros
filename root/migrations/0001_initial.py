# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-14 09:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=200, verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441')),
                ('answer', models.TextField(max_length=10000, verbose_name='\u041e\u0442\u0432\u0435\u0442')),
            ],
            options={
                'verbose_name': 'FAQ',
                'verbose_name_plural': 'FAQ',
            },
        ),
    ]
