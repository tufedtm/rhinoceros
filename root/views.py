from django.shortcuts import render
from .models import *


def index(request):
    faq = Faq.objects.all()
    works = Works.objects.all()
    workstypes = WorksType.objects.all()
    context = {
        'faq': faq,
        'works': works,
        'workstypes': workstypes
    }

    return render(request, 'index.html', context)


def contact_form(request):
    name = request.POST.get('name', '')
    phone = request.POST.get('phone', '')
    message = request.POST.get('message', '')
    ContactForm(name=name, phone=phone, message=message).save()

    return render(request, 'callback/ajax-form.html')
