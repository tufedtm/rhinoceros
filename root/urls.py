from django.conf.urls import url
import root.views

urlpatterns = [
    url(r'^$', root.views.index, name='index'),
    url(r'^contact_form$', root.views.contact_form, name='contact_form'),
]
